package com.loiane.crudspring;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import com.loiane.crudspring.model.Course;
import com.loiane.crudspring.repository.CourseRepository;

@SpringBootApplication
public class CrudSpringApplication {

	public static void main(String[] args) {
		SpringApplication.run(CrudSpringApplication.class, args);

	}
	
	
	@Bean
	public CommandLineRunner initDB(CourseRepository repository) {
		return args -> {
			repository.deleteAll();
			
			Course c1 = new Course();
			c1.setName("Java full-stack");
			c1.setCategory("Programming");
			
			repository.save(c1);
			
			
			Course c2 = new Course();
			c2.setName("Docker, Kubernetes and OpenShift");
			c2.setCategory("DevOps");
			
			repository.save(c2);
		};
	}

}
